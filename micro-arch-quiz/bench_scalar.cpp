#include <benchmark/benchmark.h>

const int kMaxSize = 10000;

template<class T>
int Init(int *a, T *b, int size) {
    int ok = 0;
    for (int i = 0; i < size; ++i) {
        a[i] = rand() % 1000;
        b[i] = rand() % 2;
        if (b[i]) {
            ok += a[i];
        }
    }
    return ok;
}

void First(benchmark::State& state) {
    const int size = kMaxSize;
    int a[size];
    bool mask[size];
    int ok = Init(a, mask, size);
    while (state.KeepRunning()) {
        int sum = 0;
        for (int i = 0; i < size; ++i) {
            if (mask[i]) {
                sum += a[i];
            }
        }
        if (sum != ok) {
            state.SkipWithError("Wrong result");
        }
    }
}

void Second(benchmark::State& state) {
    const int size = kMaxSize;
    int a[size], mask[size];
    int ok = Init(a, mask, size);
    while (state.KeepRunning()) {
        int sum = 0;
        for (int i = 0; i < size; ++i) {
            sum += mask[i] * a[i];
        }
        if (sum != ok) {
            state.SkipWithError("Wrong result");
        }
    }
}

BENCHMARK(First);
BENCHMARK(Second);

BENCHMARK_MAIN();

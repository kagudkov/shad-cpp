#include <benchmark/benchmark.h>
#include <algorithm>
#include <vector>
#include <random>

template<typename T>
class Heap {
public:
    explicit Heap(int d): child_count_(d) {}

    void push(const T& x) {
        data_.push_back(x);
        sift_up(data_.size() - 1);
    }

    const T& get_min() const {
        return data_[0];
    }

    T extract_min() {
        T result = data_[0];
        swap_index(0, data_.size() - 1);
        data_.pop_back();
        if (!empty())
            sift_down(0);
        return result;
    }

    bool empty() const {
        return data_.empty();
    }
private:
    int child_count_;
    std::vector<T> data_;

    void sift_down(size_t v) {
        if (v * child_count_ + 1 >= data_.size())
            return;
        auto begin = data_.begin() + v * child_count_ + 1;
        auto end = data_.begin() + std::min((v + 1) * child_count_ + 1, data_.size());
        size_t min = std::min_element(begin, end) - data_.begin();
        if (data_[min] < data_[v]) {
            swap_index(min, v);
            sift_down(min);
        }
    }

    void sift_up(size_t v) {
        while (v) {
            size_t p = (v - 1) / child_count_;
            if (data_[v] < data_[p]) {
                swap_index(p, v);
                v = p;
            } else
                break;
        }
    }

    void swap_index(size_t a, size_t b) {
        using std::swap;
        swap(data_[a], data_[b]);
    }
};

const int kSeed = 7475539;

std::vector<int> gen_random_permutation(int size, std::mt19937& gen) {
    std::vector<int> p(size);
    for (int i = 0; i < size; ++i)
        p[i] = i;
    std::shuffle(p.begin(), p.end(), gen);
    return p;
}

void insertions(benchmark::State& state) {
    int d = state.range(0);
    Heap<int> heap(d);
    std::mt19937 gen(kSeed);
    while (state.KeepRunning()) {
        heap.push(gen());
    }
}

int sort(const std::vector<int>& data, benchmark::State& state) {
    Heap<int> heap(state.range(1));
    for (int x : data)
        heap.push(x);
    int i = 0;
    while (!heap.empty()) {
        int x = heap.extract_min();
        if (x != i++)
            state.SkipWithError("Bad sort");
    }
    return 0;
}

void heap_sort(benchmark::State& state) {
    std::mt19937 gen(kSeed + 10);
    int size = state.range(0);
    auto data = gen_random_permutation(size, gen);
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(sort(data, state));
    }
}

BENCHMARK(insertions)->RangeMultiplier(2)->Range(2, 16);
BENCHMARK(heap_sort)->Unit(benchmark::kMicrosecond)
    ->ArgPair(100, 2)
    ->ArgPair(100, 4)
    ->ArgPair(100, 8)
    ->ArgPair(100, 16)
    ->ArgPair(10000, 2)
    ->ArgPair(10000, 4)
    ->ArgPair(10000, 8)
    ->ArgPair(10000, 16);

BENCHMARK_MAIN();

#include <benchmark/benchmark.h>

#include <atomic>
#include <vector>
#include <array>
#include <random>
#include <algorithm>
#include <cstdint>

template<int P>
static void Lermontov(benchmark::State& state) {
    std::vector<std::array<int, 1024>> data(1024);

    uint32_t j = 0;
    while(state.KeepRunning()) {
        if (P == 0) {
            data[(j >> 10) % 1024][j % 1024] = j;
        } else {
            data[j % 1024][(j >> 10) % 1024] = j;
        }
    
        j++;
    }
}

BENCHMARK_TEMPLATE(Lermontov, 0);
BENCHMARK_TEMPLATE(Lermontov, 1);

template<int Size>
static void Tolstoy(benchmark::State& state) {
    const int K = 1024 * 1024 * 8;

    std::vector<std::array<int, Size>> data(K);
    for (int i = 0; i < K; ++i) {
        data[i][0] = 10;
    }

    int total = 0;
    int i = 0;
    while(state.KeepRunning()) {
        total += data[(i++) % K][0];
    }

    benchmark::DoNotOptimize(total);    
}

BENCHMARK_TEMPLATE(Tolstoy, 1);
BENCHMARK_TEMPLATE(Tolstoy, 32);

static void Mayakovsky(benchmark::State& state) {
    const int K = 1024 * 16;
    std::vector<int> data(K);

    int step = state.range(0);

    uint32_t i = 0;
    while(state.KeepRunning()) {
        data[i % K] = 42;
    
        i += step;
    }    
}

BENCHMARK(Mayakovsky)
    ->Arg(255)
    ->Arg(256)
    ->Arg(257)
    ->Arg(511)
    ->Arg(512)
    ->Arg(513);


template<int P>
static void Dostoyevsky(benchmark::State& state) {
    const int K = 1024 * 1024 * 8;
    std::vector<uint32_t> data, queries;

    std::mt19937 gen(42);
    std::uniform_int_distribution<uint32_t> random(0, K);

    for (int i = 0; i < K; ++i) {
        data.push_back(random(gen));
        queries.push_back(random(gen));
    }

    uint32_t i = 0;
    uint32_t total = 0;
    while(state.KeepRunning()) {
        total += data[i % K];

        if (P == 0) {
            i = i * i + 5;
        } else if (P == 1) {
            i = i * data[i % K] + 5;
        } else {
            i = i * total + 5;
        }
    }
    
    benchmark::DoNotOptimize(total);
}

BENCHMARK_TEMPLATE(Dostoyevsky, 0);
BENCHMARK_TEMPLATE(Dostoyevsky, 1);
BENCHMARK_TEMPLATE(Dostoyevsky, 2);

template<int P>
static void Pushkin(benchmark::State& state) {
    std::vector<int> data, queries;

    const int N = 1024, K = 1024 * 1024 * 8;
    for (int i = 0; i < N; ++i) data.push_back(i);

    std::mt19937 gen(42);
    std::uniform_int_distribution<int> random_int(0, 1000);

    if (P == 1) {
        for (int i = 0; i < K; ++i) {
            queries.push_back(random_int(gen));
        }
    } else {
        for (int i = 0; i < K; ++i) {
            queries.push_back(1000 + random_int(gen));
        }
    }

    int total = 0;
    int i = 0;
    while(state.KeepRunning()) {
        auto it = std::lower_bound(data.begin(), data.end(),
                                   queries[i++ % K]);
                                   
        total += (it - data.begin());
    }

    benchmark::DoNotOptimize(total);
}

BENCHMARK_TEMPLATE(Pushkin, 0);
BENCHMARK_TEMPLATE(Pushkin, 1);

static std::array<std::atomic<int>, 16> global_counters;

template<int P>
static void Bulgakov(benchmark::State& state) {
    std::atomic<int> local_counter;

    while (state.KeepRunning()) {
        if (P == 0) {
            global_counters[state.thread_index]++;
        } else {
            local_counter++;
        }
    }
}

BENCHMARK_TEMPLATE(Bulgakov, 0)->Threads(1)->Threads(2);
BENCHMARK_TEMPLATE(Bulgakov, 1)->Threads(1)->Threads(2);

template<int P>
static void Gorky(benchmark::State& state) {
    const int K = 1024;

    std::vector<double> a(K, 2.6), b(K, 3.14), c(K);
    while (state.KeepRunning()) {
        if (P == 0) {
            for (int i = 0; i < K; ++i) {
                c[i] = a[i] * b[i];
            }
        } else {
            for (int i = 0; i + 2 < K; ++i) {
                c[i] = a[i + 1] * b[i + 2];
            }
        }
        
        benchmark::DoNotOptimize(c);
    }
}

BENCHMARK_TEMPLATE(Gorky, 0);
BENCHMARK_TEMPLATE(Gorky, 1);

BENCHMARK_MAIN();

import pytest
import pathlib
import subprocess


from .task import Task


@pytest.fixture
def course_root(tmpdir):
    subprocess.check_call(["cp", "-r", "grader/test_course", tmpdir])
    return pathlib.Path(tmpdir) / "test_course"


@pytest.fixture
def submit_root(tmpdir):
    subprocess.check_call(["cp", "-r", "grader/test_submits", tmpdir])
    return pathlib.Path(tmpdir) / "test_submits"


def test_task_list(course_root):
    tasks = Task.list(course_root)

    expected = set([
        "hello-world",
    ])
    
    assert expected == {t.name for t in tasks}


def test_pre_release_check(course_root):
    tasks = Task.list(course_root)

    for task in tasks:
        task.check()


def test_grade(course_root, submit_root):
    task = Task("hello-world", course_root)

    task.grade(submit_root / "hello-world-good")

    with pytest.raises(subprocess.CalledProcessError):
        task.grade(submit_root / "hello-world-failed-test")
        
    with pytest.raises(RuntimeError):
        task.grade(submit_root / "hello-world-banned-regex")

#    with pytest.raises(RuntimeError):
#        task.grade(submit_root / "hello-world-badly-formatted")

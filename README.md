# Курс по С++ в ШАД

Этот репозиторий содержит семинары и домашки для курса С++ в шаде.

[Настройка окружения](SETUP.md)

[How To Contribute](CONTRIBUTING.md)

[Программа курса](SYLLABIS.md)


## Утилиты

[ThreadSanitizer. Преобразование адреса в номер строки](tools/tsan-decoder.md)
